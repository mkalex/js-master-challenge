var canned = require('canned')
,   http = require('http')
,   opts = { cors: true, logger: process.stdout }

var fs = require('fs');
var obj = { "foo": "bar3" };
var arr = [];
var foodFreqObj = {};

var food = ['Pizza', 'Burger','Asiatisch', 'Sushi', 'Indisch', 'Mediterran',
'Orientalisch', 'Gourmet', 'International'];

function rand_range(from, to, mantissa) {
	mantissa = mantissa || 0;
    // Modified this because it was returning elements
	return (Math.random()*(to-from)+from).toFixed(mantissa);
}

var winner = rand_range(0, food.length-1);

// Since the winner is ordered with 30% probability the most ordered
// food will be food[winner]. Be aware that this is a probabilistic
// approach and there is a small probability the returned result is not
// right.
fs.writeFileSync('./canned/mostordered/index.get.json', JSON.stringify({name: food[winner]}));

function get_food_index() {
    if (Math.random() > 0.3) {
        return rand_range(0, food.length-1);
    }
    else 
    	return winner;
}

var i = 0;
setInterval(function() {
    var name = food[get_food_index()],
        geo_lat = rand_range(45, 52, 5), geo_long = rand_range(5, 30, 5),
        price = rand_range(1, 100);
	obj = { id: i, name: name, geo_lat: geo_lat, geo_long: geo_long, price: price };
    arr.push(obj);

    // This commented line right here builds a frequency object of all
    // the type of orders. If needed just pick the one with the highest
    // value and you have 100% accurate results.
    // THIS CAN HAVE AN INFLUENCE ON SPEED
    // foodFreqObj[name] = ++foodFreqObj[name] || 1;

    fs.writeFileSync('./canned/orders/index.get.json', JSON.stringify(arr));
    i++;
}, 1000);

can = canned('/canned', opts);

console.log('Listening at 3000...')
http.createServer(can).listen(3000);