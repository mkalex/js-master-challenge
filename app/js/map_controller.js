/**
 * Created by acondrea on 12/20/13.
 * Copyright: Alexandru Condrea
 */

(function($, ajax) {
    var map;
    var mapClusterer;
    var markersOnMap = 0;
    var bounds = new google.maps.LatLngBounds();
    var zoomStack = [];
    var userPressedBackBtn = false;

    $(function() {
        map = new google.maps.Map(document.getElementById("map-canvas"), _getMapOptions());

        var mostOrderedEl = document.getElementById("most-ordered");

        var mcOptions = {gridSize: 50, maxZoom: 10};
        mapClusterer = new MarkerClusterer(map, [], mcOptions);

        // Create all custom controls
        _createRefreshControl(map);
        _createBackControl(map);

        // Add initial markers to the map
        _addMarkersToMap();

        // Fill the most ordered product element
        _fillMostOrdered(mostOrderedEl);

        // Attach listener to zoom changed event to modify
        // the new bounds on the zoom stack
        google.maps.event.addListener(map, 'idle', function() {
            if (!userPressedBackBtn) {
                var center = this.getCenter();
                zoomStack.push(this.getZoom());
                zoomStack.push(center);
            } else {
                userPressedBackBtn = false;
            }
        });
    });

    function _getMapOptions() {
        return {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            disableDoubleClickZoom: true,
            draggable: false,
            scrollwheel: false,
            styles: [
                {
                    "featureType": "poi",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "transit",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "administrative.neighborhood",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "landscape",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "road.arterial",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "road.local",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "administrative.land_parcel",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "administrative.neighborhood",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "administrative.province",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "administrative.locality",
                    "elementType": "labels.icon",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                },{
                    "featureType": "road.highway",
                    "stylers": [
                        { "visibility": "simplified" },
                        { "hue": "#0091ff" },
                        { "saturation": -16 },
                        { "gamma": 0.54 },
                        { "weight": 0.3 }
                    ]
                },{
                    "featureType": "landscape.natural.landcover",
                    "elementType": "geometry",
                    "stylers": [
                        { "visibility": "on" },
                        { "saturation": -72 },
                        { "gamma": 1.42 },
                        { "lightness": 20 },
                        { "hue": "#eeff00" }
                    ]
                }
            ]
        };
    }

    function _fillMostOrdered(element) {
        ajax.ajaxGetJSON('http://localhost:3000/mostordered', function(data) {
            if (typeof data === "object" && data instanceof Object) {
                var mostOrdered = data.name;
                element.innerHTML = mostOrdered;
            }
        });
    }

    function _createRefreshControl(map) {
        // Create a div to hold the control.
        var controlDiv = document.getElementById('btn-refresh');

        // Setup the click event
        google.maps.event.addDomListener(controlDiv, 'click', _addMarkersToMap);

        // Put the control on the map
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);
    }

    function _createBackControl(map) {
        // Create a div to hold the control.
        var controlDiv = document.getElementById('btn-back');

        // Setup the click event
        google.maps.event.addDomListener(controlDiv, 'click', _goBackOneStep);

        // Put the control on the map
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(controlDiv);
    }

    function _addMarkersToMap() {
        var oldInfoWindow;
        ajax.ajaxGetJSON('http://localhost:3000/orders', function(data) {
            if (typeof data === "object" && data instanceof Array) {
                var markers = [];
                var dataLength = data.length;

                for (var i = markersOnMap; i < dataLength; i++) {
                    var latLng = new google.maps.LatLng(data[i].geo_lat,
                        data[i].geo_long);
                    bounds.extend(latLng);

                    var marker = new google.maps.Marker({'position': latLng});
                    marker.data = data[i];
                    marker.index = i;

                    markers.push(marker);

                    google.maps.event.addListener(marker, 'click', function() {
                        if (oldInfoWindow)
                            oldInfoWindow.close();
                        var infoWindow = new google.maps.InfoWindow({
                            position: this.position,
                            content: '<div class="info-wnd-content">' +
                                '<span class="name">'+ this.data.name + '</span>' +
                                '<span class="price">'+ this.data.price +
                                ' <i class="fa fa-eur"></i>' +
                                '</span>' +
                                '</div>'
                        });
                        oldInfoWindow = infoWindow;
                        infoWindow.open(map, this);

                    });
                }

                google.maps.event.addListener(map, 'back-btn-pressed', function() {
                    if (oldInfoWindow)
                        oldInfoWindow.close();
                });

                // Empty the zoom stack
                if (zoomStack.length != 2) {
                    zoomStack = [];
                }

                map.fitBounds(bounds);
                mapClusterer.addMarkers(markers);
                markersOnMap = dataLength;
            }
        });
    }

    function _goBackOneStep() {
        if (zoomStack.length != 2) {
            userPressedBackBtn = true;
            google.maps.event.trigger(map, 'back-btn-pressed');
            zoomStack.pop();
            zoomStack.pop();
            map.setCenter(zoomStack[zoomStack.length - 1]);
            map.setZoom(zoomStack[zoomStack.length - 2]);
        }
    }
}(window.$, ajax));