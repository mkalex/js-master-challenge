/**
 * Created by acondrea on 12/20/13.
 * Copyright: Alexandru Condrea
 */

var ajax = (function(ajax) {
    // Returns a JSON Object (could be an array) with the requested data
    function _ajaxGetJSON(url, successCallback) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", url, true);

        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                if (successCallback && typeof(successCallback) === "function") {
                    if (xmlHttp.getResponseHeader("Content-Type") === "application/json") {
                        var data = JSON.parse(xmlHttp.responseText);
                        successCallback(data);
                    }
                }
            }
        }

        xmlHttp.send();
    }

    ajax.ajaxGetJSON = _ajaxGetJSON;
    return ajax;

}(ajax || {}));
